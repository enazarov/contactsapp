//
//  ContactViewModelTests.m
//  ContactsApp
//
//  Created by Evgeny on 19.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ContactDetailViewModel.h"
#import "ContactListViewModel.h"
#import "DataController.h"
#import "NSString+Triming.h"

@interface ContactViewModelTests : XCTestCase
@property (nonatomic) ContactListViewModel *listViewModel;
@end

@implementation ContactViewModelTests

- (void)setUp {
    [super setUp];
    DataController *dataController = [DataController sharedInstance];
    self.listViewModel = [[ContactListViewModel alloc] initWithDataController:dataController];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.listViewModel = nil;
    [super tearDown];
}

- (void)testContactDetailValidation {
    ContactDetailViewModel *viewModel = [self.listViewModel newContactViewModel];
    
    viewModel.firstName = @"a";
    viewModel.lastName = @"abc";
    viewModel.phoneNumber = @"+442012341234";
    
    BOOL success = NO;
    NSNumber *valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertTrue(valid.boolValue, @"Incorrect contact view model validation");
    
    viewModel.firstName = @"";
    valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertFalse(valid.boolValue, @"Incorrect contact view model validation");
    
    viewModel.firstName = @"a";
    viewModel.lastName = @" ";
    valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertFalse(valid.boolValue, @"Incorrect contact view model validation");
    
    viewModel.firstName = @"b";
    viewModel.phoneNumber = @"+79031234567";
    
    valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertFalse(valid.boolValue, @"Incorrect contact view model validation");
    
    viewModel.phoneNumber = @"+7903";
    valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertFalse(valid.boolValue, @"Incorrect contact view model validation");
    
    viewModel.phoneNumber = @"";
    valid = [viewModel.formValidSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertFalse(valid.boolValue, @"Incorrect contact view model validation");
}

- (void)testStringTriming {
    NSString *source = @"  Abc d e ";
    NSString *string = [source trimWhitespacesAllowTailingSpace:YES];
    XCTAssertEqualObjects(string, @"Abc d e ", @"Incorrect NSString triming");
    
    NSString *string1 = [source trimWhitespacesAllowTailingSpace:NO];
    XCTAssertEqualObjects(string1, @"Abc d e", @"Incorrect NSString triming");
}

- (void)testContactList {
    //Test create contact
    NSString *string = @"A B C D E F G H I K L M N";
    NSArray *lastNames = [string componentsSeparatedByString:@" "];
    for (NSString *lastName in lastNames) {
        RACSignal *createSignal = [self.listViewModel addNewContactWithFirstName:@"First"
                                                                        lastName:lastName
                                                                     phoneNumber:@"+44 20 1234 1234"];
        BOOL success = NO;
        [createSignal asynchronousFirstOrDefault:nil success:&success error:nil];
        XCTAssertTrue(success, @"Contact create signal is not succesfull");
    }
    
    XCTAssertEqual(lastNames.count, self.listViewModel.count, @"Incorrect contact list count");
    
    //Test contact deletion
    NSUInteger indexToRemove = arc4random_uniform((u_int32_t) self.listViewModel.count);
    id<ContactProtocol> contact = self.listViewModel.contacts[indexToRemove];
    RACSignal *deleteSignal = [self.listViewModel deleteContact:contact];
    BOOL success = NO;
    [deleteSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertTrue(success, @"Contact delete signal is not succesfull");
    XCTAssertEqual(lastNames.count-1, self.listViewModel.count, @"Incorrect contact list count");
    
    //Test contact update
    NSUInteger indexToUpdate = arc4random_uniform((u_int32_t) self.listViewModel.count);
    id<ContactProtocol> contactToUpdate = self.listViewModel.contacts[indexToUpdate];
    RACSignal *updateSignal = [self.listViewModel updateContact:contactToUpdate
                                                  withFirstName:@"X"
                                                       lastName:@"XXX"
                                                    phoneNumber:@"+44 20 1234 1235"];
    
    success = NO;
    NSNumber *newIndex = [updateSignal asynchronousFirstOrDefault:nil success:&success error:nil];
    XCTAssertTrue(success, @"Contact update signal is not succesfull");
    
    id<ContactProtocol> updatedContact = self.listViewModel.contacts[newIndex.unsignedIntegerValue];
    
    XCTAssertEqualObjects(updatedContact.firstName, @"X", @"Incorect contact first name update");
    XCTAssertEqualObjects(updatedContact.lastName, @"XXX", @"Incorect contact last name update");
    XCTAssertEqualObjects(updatedContact.phoneNumber, @"+44 20 1234 1235", @"Incorect contact phone number update");
}

@end
