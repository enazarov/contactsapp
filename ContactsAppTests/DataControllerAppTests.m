//
//  ContactsAppTests.m
//  ContactsAppTests
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Contact.h"
#import "DataController.h"

@interface DataControllerAppTests : XCTestCase
@property (nonatomic) DataController *dataController;
@end

@implementation DataControllerAppTests

- (void)setUp {
    [super setUp];
    self.dataController = [[DataController alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.dataController = nil;
    [super tearDown];
}

- (void)testContactFullName {
    Contact *contact = [Contact new];
    contact.firstName = @"First";
    contact.lastName = @"Last";
    
    XCTAssertEqualObjects(contact.fullName, @"First Last", @"Incorrect contact full name");
}

- (void)testDataControllerCreateContact {
    
    [self.dataController addNewContactWithFirstName:@"Alex" lastName:@"A" phoneNumber:nil error:nil];
    [self.dataController addNewContactWithFirstName:@"Cesar" lastName:@"C" phoneNumber:nil error:nil];
    NSUInteger index = [self.dataController addNewContactWithFirstName:@"Boris" lastName:@"B" phoneNumber:nil error:nil];
    
    XCTAssertEqual(index, 1, @"Incorrect index if new contact in Data Controller");
}

- (void)testDataControllerUpdateContact {
    NSInteger index = [self.dataController addNewContactWithFirstName:@"Contact" lastName:@"1" phoneNumber:@"" error:nil];
    Contact *contact = [self.dataController allContacts][index];

    index = [self.dataController updateContact:contact withFirstName:@"ABC" lastName:@"DEF" phoneNumber:@"123" error:nil];
    XCTAssertNotEqual(index, NSNotFound, @"Contact update doesn't work");
    
    contact = [self.dataController allContacts][index];
    
    XCTAssertEqualObjects(contact.firstName, @"ABC", @"Incorrect contact first name");
    XCTAssertEqualObjects(contact.lastName, @"DEF", @"Incorrect contact last name");
    XCTAssertEqualObjects(contact.phoneNumber, @"123", @"Incorrect contact phone number");
}


- (void)testDataControllerDeleteContact {
    
    [self.dataController addNewContactWithFirstName:@"Contact" lastName:@"1" phoneNumber:@"" error:nil];
    NSInteger index = [self.dataController addNewContactWithFirstName:@"Contact" lastName:@"2" phoneNumber:@"" error:nil];
    
    NSArray *dataArray = [self.dataController allContacts];
    Contact *contact = dataArray[index];
    
    NSUInteger index1 = [self.dataController deleteContact:contact error:nil];
    XCTAssertEqual(index, index1, @"Incorrect index of deleted contact");
    
    
    NSError *error;
    index = [self.dataController deleteContact:contact error:&error];
    XCTAssertEqual(index, NSNotFound, @"Incorrect deleted contact index");
    XCTAssertNotNil(error, @"Contact should be deleted successful");
}



@end
