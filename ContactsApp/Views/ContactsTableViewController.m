//
//  ContactsTableViewController.m
//  ContactsApp
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "ContactsTableViewController.h"
#import "ContactDetailViewController.h"
#import "ContactListViewModel.h"
#import "DataController.h"
#import "UIViewController+Error.h"

static NSString *kContactDetailsSegueId = @"showContact";

@interface ContactsTableViewController ()

@end

@implementation ContactsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewModel];
    
    self.title = NSLocalizedString(@"Contacts", nil);
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                  target:self
                                                                                  action:@selector(addButtonTapped:)];
    self.navigationItem.rightBarButtonItem = addBarButton;
    self.tableView.rowHeight = 55.f;
    UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), self.tableView.rowHeight)];
    countLabel.backgroundColor = [UIColor whiteColor];
    countLabel.textAlignment = NSTextAlignmentCenter;
    self.tableView.tableFooterView = countLabel;
    
    RAC(countLabel, text) = [RACObserve(self.viewModel, contacts)
                             map:^id(NSArray *array) {
        if (array.count) {
            return [NSString stringWithFormat:@"%@: %lu", NSLocalizedString(@"Contacts count", nil), (unsigned long)array.count];
        }
        else {
            return NSLocalizedString(@"No contacts", nil);
        }
    }];
    
}

- (void)setupViewModel {
    self.viewModel = [[ContactListViewModel alloc] initWithDataController:[DataController sharedInstance]];
    
    @weakify(self);
    [self.viewModel.createSignal subscribeNext:^(NSNumber *index) {
        @strongify(self);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index.unsignedIntegerValue inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
    
    [self.viewModel.updateSignal subscribeNext:^(NSNumber *index) {
        @strongify(self);
        [self.tableView reloadData];
    }];
    
    [self.viewModel.deleteSignal subscribeNext:^(NSNumber *index) {
        @strongify(self);
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index.unsignedIntegerValue inSection:0];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)addButtonTapped:(id)sender {
    [self performSegueWithIdentifier:kContactDetailsSegueId
                              sender:[self.viewModel newContactViewModel]];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellId = @"ContactCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellId];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.textColor = [UIColor grayColor];
    }
    
    id<ContactProtocol>contact = self.viewModel.contacts[indexPath.row];
    
    cell.textLabel.attributedText = [self fullNameStringWithContact:contact];
    cell.detailTextLabel.text = contact.phoneNumber;
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:kContactDetailsSegueId
                              sender:[self.viewModel contactViewModelAtIndex:indexPath.row]];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        @weakify(self);
        [[self.viewModel deleteContactAtIndex:indexPath.row]
         subscribeError:^(NSError *error) {
             @strongify(self);
             [self showError:error];
        }];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(ContactDetailViewModel *)viewModel {
    if ([segue.identifier isEqualToString:kContactDetailsSegueId]) {
        ContactDetailViewController *destinationViewControlller = segue.destinationViewController;
        destinationViewControlller.viewModel = viewModel;
    }
}

#pragma mark - Helpers
- (NSAttributedString *)fullNameStringWithContact:(id<ContactProtocol>)contact {
    NSMutableAttributedString *result = [NSMutableAttributedString new];
    
    if (contact.firstName && ![contact.firstName isEqual:[NSNull null]] && contact.firstName.length) {
        NSAttributedString *firstName = [[NSAttributedString alloc] initWithString:[contact.firstName stringByAppendingString:@" "]
                                                                        attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17.f]}];
        [result appendAttributedString:firstName];
    }
    
    if (contact.lastName && ![contact.lastName isEqual:[NSNull null]]) {
        NSAttributedString *firstName = [[NSAttributedString alloc] initWithString:contact.lastName
                                                                        attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17.f]}];
        [result appendAttributedString:firstName];
    }
    
    return result.copy;
}



@end
