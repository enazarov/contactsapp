//
//  ContactsTableViewController.h
//  ContactsApp
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ContactListViewModel;
@interface ContactsTableViewController : UITableViewController
@property (nonatomic) ContactListViewModel *viewModel;
@end
