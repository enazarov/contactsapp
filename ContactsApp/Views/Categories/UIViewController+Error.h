//
//  UIViewController+Error.h
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Error)
- (void)showError:(NSError *)error;
@end
