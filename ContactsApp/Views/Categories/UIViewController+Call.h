//
//  UIViewController+Call.h
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Call)
- (RACSignal *)callPhoneNumber:(NSString *)phoneNumber;
@end
