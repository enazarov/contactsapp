//
//  UIViewController+Error.m
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "UIViewController+Error.h"

@implementation UIViewController (Error)
- (void)showError:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil)
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
    {
         [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
