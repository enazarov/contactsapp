//
//  UIViewController+Call.m
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "UIViewController+Call.h"

@implementation UIViewController (Call)
- (RACSignal *)callPhoneNumber:(NSString *)phoneNumber {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSString *normalizedNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt:" stringByAppendingString:normalizedNumber]];
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
            [subscriber sendNext:@YES];
            [subscriber sendCompleted];
        }
        else {
            NSError *error = [[NSError alloc] initWithDomain:@"ru.enazarov.contactApp.phoneCall"
                                                        code:10
                                                    userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Your device doesn't support phone calls.", nil)}];
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }
        
        return nil;
    }];
    
}
@end
