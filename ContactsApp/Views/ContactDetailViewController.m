//
//  ContactViewController.m
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "ContactDetailViewModel.h"
#import "PhoneNumberTableViewCell.h"
#import "UIViewController+Error.h"
#import "UIViewController+Call.h"

static NSString *kTextFieldCellId       = @"TextFieldCell";
static NSString *kDefaultCellId         = @"DefaultCell";
static NSString *kPhoneNumberCellId     = @"PhoneNumberCell";
static NSString *kDeleteButtonCellId    = @"DeleteButtonCell";

static NSInteger kTextFieldTag      = 1001;
static NSInteger kDeleteLabelTag    = 1002;

@interface ContactDetailViewController () <UITextFieldDelegate>
@property (nonatomic) UIBarButtonItem *cancelButton;
@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Contact", nil);
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.editButtonItem.target = self;
    self.editButtonItem.action = @selector(editButtonTapped:);
    
    self.cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                      target:self
                                                                      action:@selector(cancelEditing:)];
    
    self.tableView.tableFooterView = [UIView new]; //Remove empty rows separators
    
    if (self.viewModel.isNewContact) {
        [self setEditing:YES animated:NO];
    }
    
    RAC(self.editButtonItem, enabled) = [RACSignal combineLatest:@[self.viewModel.formValidSignal,
                                                                   RACObserve(self, isEditing)]
                                                          reduce:^(NSNumber *valid, NSNumber *editing) {
                                                              if (editing.boolValue) {
                                                                  return @(valid.boolValue);
                                                              }
                                                              else {
                                                                  return @YES;
                                                              }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing) {
        [self.navigationItem setLeftBarButtonItem:self.cancelButton animated:animated];
    }
    else {
        [self.navigationItem setLeftBarButtonItem:self.navigationItem.backBarButtonItem animated:animated];
    }
    
    [self.tableView reloadData];
}

- (void)cancelEditing:(id)sender {
    if (self.viewModel.isNewContact) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self setEditing:NO animated:YES];
    }
}

- (void)editButtonTapped:(id)sender {
    if (self.isEditing) {
        @weakify(self);
        [self.viewModel.saveSignal subscribeNext:^(id x) {
            @strongify(self);
            [self setEditing:NO animated:YES];
        } error:^(NSError *error) {
            @strongify(self);
            [self showError:error];
        }];
    }
    else {
        [self setEditing:YES animated:YES];
    }
}

- (void)showDeleteConfirmationActionSheet {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                    style:UIAlertActionStyleCancel
                                                  handler:nil]];
    @weakify(self);
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Delete contact", nil)
                                                    style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                            {
                                @strongify(self);
                                [self.viewModel.deleteSignal
                                 subscribeNext:^(id x) {
                                     @strongify(self);
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }
                                 error:^(NSError *error) {
                                     @strongify(self);
                                     [self showError:error];
                                 }];
                            }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.isEditing && !self.viewModel.isNewContact) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (self.isEditing) {
            return [self tableView:tableView configureTextFieldCellAtIndexPath:indexPath];
        }
        else {
            return [self tableView:tableView contactCellForRowAtIndexPath:indexPath];
        }
    }
    
    return [self tableView:tableView deleteCellWithIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView configureTextFieldCellAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTextFieldCellId forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UITextField *textField = [cell viewWithTag:kTextFieldTag];
    textField.delegate = self;
    NSString *keypath = nil;
    switch (indexPath.row) {
        case 0:
            textField.placeholder = NSLocalizedString(@"First name", nil);
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.returnKeyType = UIReturnKeyNext;
            keypath = @keypath(self.viewModel, firstName);
            break;
        case 1:
            textField.placeholder = NSLocalizedString(@"Last name", nil);
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.returnKeyType = UIReturnKeyNext;
            keypath = @keypath(self.viewModel, lastName);
            break;
        case 2:
        default:
            textField.placeholder = NSLocalizedString(@"Phone number", nil);
            textField.keyboardType = UIKeyboardTypePhonePad;
            textField.returnKeyType = UIReturnKeyDefault;
            keypath = @keypath(self.viewModel, phoneNumber);
            break;
    }
    
    [self bindCell:cell textField:textField withKeyPath:keypath];
    
    return cell;
}

- (void)bindCell:(UITableViewCell *)cell textField:(UITextField *)textField withKeyPath:(NSString *)keyPath
{
    RAC(textField, text) = [[self.viewModel rac_valuesForKeyPath:keyPath observer:cell]
                            takeUntil:[cell rac_prepareForReuseSignal]];
    
    @weakify(self);
    [[[textField rac_textSignal] takeUntil:[cell rac_prepareForReuseSignal]]
     subscribeNext:^(NSString *text) {
         @strongify(self);
         [self.viewModel setValue:text forKey:keyPath];
     }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView deleteCellWithIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDeleteButtonCellId forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UILabel *label = [cell viewWithTag:kDeleteLabelTag];
    label.text = NSLocalizedString(@"Delete contact", nil);
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView contactCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    switch (indexPath.row) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:kDefaultCellId forIndexPath:indexPath];
            cell.textLabel.text = self.viewModel.firstName;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            break;
        case 1:
            cell = [tableView dequeueReusableCellWithIdentifier:kDefaultCellId forIndexPath:indexPath];
            cell.textLabel.text = self.viewModel.lastName;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            break;
        case 2:
        default:
            cell = [tableView dequeueReusableCellWithIdentifier:kPhoneNumberCellId forIndexPath:indexPath];
            [(PhoneNumberTableViewCell *)cell label].text = self.viewModel.phoneNumber;
            break;
    }
    
    return cell;
}


#pragma mark - Table View Delegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && self.isEditing && !self.viewModel.isNewContact) {
        [self showDeleteConfirmationActionSheet];
    }
    else if (!self.isEditing && indexPath.row == 2) {
        @weakify(self, tableView);
        [[self callPhoneNumber:self.viewModel.phoneNumber]
         subscribeError:^(NSError *error)
         {
             @strongify(self, tableView);
             [self showError:error];
             [tableView deselectRowAtIndexPath:indexPath animated:YES];
         } completed:^
         {
             @strongify(tableView);
             [tableView deselectRowAtIndexPath:indexPath animated:YES];
         }];
    }
}

- (void)dealloc {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITableViewCell * currentCell = (UITableViewCell *) textField.superview.superview;
    NSIndexPath *currentIndexPath = [self.tableView indexPathForCell:currentCell];
    
    if (currentIndexPath.row != [self tableView:self.tableView numberOfRowsInSection:0]-1) {
        NSIndexPath * nextIndexPath = [NSIndexPath indexPathForRow:currentIndexPath.row + 1 inSection:0];
        UITableViewCell *nextCell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:nextIndexPath];
        UITextField *textField = [nextCell viewWithTag:kTextFieldTag];
        [textField becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}

@end
