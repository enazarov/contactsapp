//
//  ContactViewController.h
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ContactDetailViewModel;
@interface ContactDetailViewController : UITableViewController
@property (nonatomic) ContactDetailViewModel *viewModel;
@end
