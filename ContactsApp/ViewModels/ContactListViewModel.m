//
//  ContactsViewModel.m
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "ContactListViewModel.h"
#import "ContactDetailViewModel.h"
#import "DataController.h"
#import "Contact.h"

@interface ContactListViewModel ()
@property (nonatomic) DataController *dataController;
@property (nonatomic) NSArray *contacts;
@property (nonatomic) RACSubject *updateSignal;
@property (nonatomic) RACSubject *createSignal;
@property (nonatomic) RACSubject *deleteSignal;
@end

@implementation ContactListViewModel

- (instancetype)initWithDataController:(DataController *)dataController {
    self = [super init];
    if (self) {
        self.dataController = dataController;
        self.updateSignal = [[RACSubject subject] setNameWithFormat:@"ContactsViewModel updateSignal"];
        self.createSignal = [[RACSubject subject] setNameWithFormat:@"ContactsViewModel createSignal"];
        self.deleteSignal = [[RACSubject subject] setNameWithFormat:@"ContactsViewModel deleteSignal"];
        
        [self refreshData];
        
        return self;
    }
    
    return nil;
}

- (void)refreshData {
    self.contacts = self.dataController.allContacts;
}

- (NSUInteger)count {
    return self.contacts.count;
}

- (ContactDetailViewModel *)newContactViewModel {
    ContactDetailViewModel *childViewModel = [[ContactDetailViewModel alloc] initWithContact:nil];
    childViewModel.parentViewModel = self;
    return childViewModel;
}

- (ContactDetailViewModel *)contactViewModelAtIndex:(NSUInteger)index {
    if (index>=self.dataController.allContacts.count) {
        return nil;
    }
    
    id<ContactProtocol> contact = self.dataController.allContacts[index];;
    ContactDetailViewModel *viewModel = [[ContactDetailViewModel alloc] initWithContact:contact];
    viewModel.parentViewModel = self;
    return viewModel;
}

#pragma mark - Contact
- (RACSignal *)addNewContactWithFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSError *error = nil;
        NSUInteger index = [self.dataController addNewContactWithFirstName:firstName
                                                                  lastName:lastName
                                                               phoneNumber:phoneNumber
                                                                     error:&error];
        if (error) {
            [subscriber sendError:error];
        }
        else {
            [self refreshData];
            [self.createSignal sendNext:@(index)];
            [subscriber sendNext:self.contacts[index]];
            [subscriber sendCompleted];
        }
        
        return nil;
    }];
    
}

- (RACSignal *)deleteContactAtIndex:(NSUInteger)index {
    NSAssert(index<self.count, @"Wrong index of contact");
    id<ContactProtocol>contact = self.contacts[index];
    return [self deleteContact:contact];
}

- (RACSignal *)deleteContact:(id<ContactProtocol>)contact {
    NSAssert(contact, @"Trying to delete a nil contact");
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSError *error = nil;
        NSUInteger index = [self.dataController deleteContact:contact error:&error];
        if (error) {
            [subscriber sendError:error];
        }
        else {
            [self refreshData];
            [self.deleteSignal sendNext:@(index)];
            [subscriber sendNext:@(index)];
            [subscriber sendCompleted];
        }
        return nil;
    }];
}

- (RACSignal *)updateContact:(id<ContactProtocol>)contact
               withFirstName:(NSString *)firstName
                    lastName:(NSString *)lastName
                 phoneNumber:(NSString *)phoneNumber
{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSError *error = nil;
        NSUInteger index = [self.dataController updateContact:contact
                                                withFirstName:firstName
                                                     lastName:lastName
                                                  phoneNumber:phoneNumber
                                                        error:&error];
        if (error) {
            [subscriber sendError:error];
        }
        else {
            [self refreshData];
            [self.updateSignal sendNext:@(index)];
            [subscriber sendNext:@(index)];
            [subscriber sendCompleted];
        }
        
        return nil;
    }];
}

@end
