//
//  ContactViewModel.m
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "ContactDetailViewModel.h"
#import "ContactListViewModel.h"
#import <libPhoneNumber-iOS/NBPhoneNumberUtil.h>
#import <libPhoneNumber-iOS/NBPhoneNumber.h>
#import "NSString+Triming.h"

@interface ContactDetailViewModel ()
@property (nonatomic) NBPhoneNumberUtil *phoneNumberUtil;
@property (nonatomic) NSString *defaultCountryCode;
@property (nonatomic, getter=isPhoneNumberValid) BOOL phoneNumberValid;
- (BOOL)hasChanges;
@end

@implementation ContactDetailViewModel

- (instancetype)initWithContact:(id<ContactProtocol>)contact {
    self = [super init];
    if (self) {
        if (contact) {
            self.contact = contact;
            
            self.firstName = [contact.firstName copy];
            self.lastName = [contact.lastName copy];
            self.phoneNumber = [contact.phoneNumber copy];
        }
        else {
            self.newContact = YES;
        }
    
        self.defaultCountryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
        self.phoneNumberUtil = [[NBPhoneNumberUtil alloc] init];
        self.formValidSignal = [RACSignal
                                combineLatest:@[
                                                RACObserve(self, firstName),
                                                RACObserve(self, lastName),
                                                RACObserve(self, phoneNumberValid)
                                                ]
                                reduce:^(NSString *firstName, NSString *lastName, NSNumber *phoneNumberValid) {
                                    return @(firstName.length &&
                                              lastName.length &&
                                   phoneNumberValid.boolValue);
                                }];
        
        return self;
    }
    
    return nil;
}

#pragma mark - Setters
- (void)setPhoneNumber:(NSString *)phoneNumber {
    [super willChangeValueForKey:@keypath(self, phoneNumber)];
    _phoneNumber = [self formatPhoneNumberString:phoneNumber];
    [super didChangeValueForKey:@keypath(self, phoneNumber)];
}

- (void)setFirstName:(NSString *)firstName {
    [super willChangeValueForKey:@keypath(self, firstName)];
    _firstName = [firstName trimWhitespacesAllowTailingSpace:YES];
    [super didChangeValueForKey:@keypath(self, firstName)];
}

- (void)setLastName:(NSString *)lastName {
    [super willChangeValueForKey:@keypath(self, lastName)];
    _lastName = [lastName trimWhitespacesAllowTailingSpace:YES];
    [super didChangeValueForKey:@keypath(self, lastName)];
}

#pragma mark - Signals

- (RACSignal *)saveSignal {
    NSString *trimedFirstName = [self.firstName trimWhitespacesAllowTailingSpace:NO];
    NSString *trimedLastName = [self.lastName trimWhitespacesAllowTailingSpace:NO];
    if (self.isNewContact) {
        @weakify(self);
        return [[self.parentViewModel addNewContactWithFirstName:trimedFirstName
                                                       lastName:trimedLastName
                                                    phoneNumber:self.phoneNumber]
                doNext:^(id <ContactProtocol> contact) {
                    @strongify(self);
                    self.contact = contact;
                    self.newContact = NO;
                }];
    }
    else {
        if ([self hasChanges]) {
            return [self.parentViewModel updateContact:self.contact
                                         withFirstName:trimedFirstName
                                              lastName:trimedLastName
                                           phoneNumber:self.phoneNumber];
        }
        else {
            return [RACSignal return:@YES];
        }
    }
}

- (RACSignal *)deleteSignal {
    return [self.parentViewModel deleteContact:self.contact];
}

#pragma mark - Utils

- (NSString *)formatPhoneNumberString:(NSString *)phoneNumber {
    NBPhoneNumber *phoneNumberObj = [self.phoneNumberUtil parse:phoneNumber
                                                  defaultRegion:self.defaultCountryCode
                                                          error:nil];
    NSError *error;
    NSString *phoneNumberString = [self.phoneNumberUtil format:phoneNumberObj
                                                  numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                                                         error:&error];
    
    self.phoneNumberValid = [self.phoneNumberUtil isValidNumber:phoneNumberObj];
    
    if (phoneNumberString) {
        return phoneNumberString;
    }
    else {
        return phoneNumber;
    }
}

- (BOOL)hasChanges {
    if (!self.contact) {
        return YES;
    }
    else if (![self.firstName isEqualToString:self.contact.firstName]) {
        return YES;
    }
    else if (![self.lastName isEqualToString:self.contact.lastName]) {
        return YES;
    }
    else if (![self.phoneNumber isEqualToString:self.contact.phoneNumber]) {
        return YES;
    }
    
    return NO;
}

- (void)dealloc {
    
}

@end
