//
//  ContactsViewModel.h
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactProtocol.h"
@class DataController, ContactDetailViewModel, RACSubject, RACSignal, Contact;
@interface ContactListViewModel : NSObject
@property (nonatomic) ContactDetailViewModel *childViewModel;
@property (nonatomic, readonly) DataController *dataController;
@property (nonatomic, readonly) NSArray *contacts;
@property (nonatomic, readonly) RACSubject *updateSignal;
@property (nonatomic, readonly) RACSubject *createSignal;
@property (nonatomic, readonly) RACSubject *deleteSignal;

- (instancetype)initWithDataController:(DataController *)dataController;
- (NSUInteger)count;
- (ContactDetailViewModel *)newContactViewModel;
- (ContactDetailViewModel *)contactViewModelAtIndex:(NSUInteger)index;
- (RACSignal *)addNewContactWithFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber;
- (RACSignal *)deleteContactAtIndex:(NSUInteger)index;
- (RACSignal *)deleteContact:(id<ContactProtocol>)contact;
- (RACSignal *)updateContact:(id<ContactProtocol>)contact withFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber;

@end
