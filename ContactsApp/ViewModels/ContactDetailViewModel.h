//
//  ContactViewModel.h
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactProtocol.h"

@class ContactListViewModel;
@interface ContactDetailViewModel : NSObject
@property (nonatomic, weak) ContactListViewModel *parentViewModel;
@property (nonatomic, readonly) NSUInteger count;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) id<ContactProtocol> contact;
@property (nonatomic, getter=isNewContact) BOOL newContact;
@property (nonatomic) RACSignal *formValidSignal;

- (instancetype)initWithContact:(id<ContactProtocol>)contact;

- (RACSignal *)saveSignal;
- (RACSignal *)deleteSignal;

- (NSString *)formatPhoneNumberString:(NSString *)phoneNumber;

@end
