//
//  PhoneNumberTableViewCell.m
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "PhoneNumberTableViewCell.h"

@implementation PhoneNumberTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.callImageView.tintColor = self.tintColor;
    self.label.textColor = self.tintColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
