//
//  ContactProtocol.h
//  ContactsApp
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ContactProtocol <NSObject>
@required

/**
 *  First name
 */
- (NSString *)firstName;

/**
 *  Last name
 */
- (NSString *)lastName;

/**
 *  International phone number
 */
- (NSString *)phoneNumber;

/**
 *  @return Full name of a customer
 */
- (NSString *)fullName;

@end
