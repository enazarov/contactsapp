//
//  Contact.m
//  ContactsApp
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (NSString *)fullName {
    NSMutableString *string = [NSMutableString new];
    
    if (self.firstName && ![self.firstName isEqual:[NSNull null]] && self.firstName.length) {
        [string appendString:self.firstName];
        [string appendString:@" "];
    }
    
    if (self.lastName && ![self.lastName isEqual:[NSNull null]]) {
        [string appendString:self.lastName];
    }
    
    return string.copy;
}

@end
