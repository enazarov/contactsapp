//
//  Contact.h
//  ContactsApp
//
//  Created by Evgeny on 14.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactProtocol.h"
@interface Contact : NSObject <ContactProtocol>
/**
 *  First name
 */
@property (nonatomic, copy) NSString *firstName;
/**
 *  Last name
 */
@property (nonatomic, copy) NSString *lastName;
/**
 *  International phone number
 */
@property (nonatomic, copy) NSString *phoneNumber;

@end
