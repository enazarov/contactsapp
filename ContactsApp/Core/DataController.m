//
//  DataController.m
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "DataController.h"
#import "Contact.h"

@interface DataController ()
@property (nonatomic) NSMutableArray *contacts; //Of Contact
@property (nonatomic) dispatch_queue_t queue;
@end

@implementation DataController

+ (instancetype)sharedInstance {
    static DataController *controller = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        controller = [[DataController alloc] init];
    });
    
    return controller;
}


- (instancetype)init {
    self = [super init];
    if (self) {
        self.contacts = [NSMutableArray array];
        self.queue = dispatch_queue_create("ru.enazarov.contactsApp.dataController", DISPATCH_QUEUE_SERIAL);
        return self;
    }
    
    return nil;
}

#pragma mark - Contacts
- (NSArray *)allContacts {
    __block NSArray *contacts = nil;
    dispatch_sync(self.queue, ^{
        contacts = self.contacts.copy;
    });
        
    return contacts;
}

#pragma mark Create
- (NSUInteger)addNewContactWithFirstName:(NSString *)firstName
                                lastName:(NSString *)lastName
                             phoneNumber:(NSString *)phoneNumber
                                   error:(NSError *__autoreleasing *)error
{
    Contact *contact = [[Contact alloc] init];
    return [self updateContact:contact
                 withFirstName:firstName
                      lastName:lastName
                   phoneNumber:phoneNumber
                         error:error];
}

#pragma mark Delete
- (NSUInteger)deleteContact:(id<ContactProtocol>)contact error:(NSError *__autoreleasing *)error {
    __block NSUInteger index;
    dispatch_sync(self.queue, ^{
        index = [self.contacts indexOfObject:contact];
        if (index == NSNotFound) {
            *error = [NSError errorWithDomain:@"ru.enazarov.contactApp.dataController"
                                         code:10
                                     userInfo:@{NSLocalizedDescriptionKey:NSLocalizedString(@"Contact entity is not presented in the store.", nil)}];
        }
        else {
            [self.contacts removeObjectAtIndex:index];
        }
    });
    
    return index;
}

#pragma mark Update
- (NSUInteger)updateContact:(id<ContactProtocol>)contact
              withFirstName:(NSString *)firstName
                   lastName:(NSString *)lastName
                phoneNumber:(NSString *)phoneNumber
                      error:(NSError *__autoreleasing *)error
{
    __block NSUInteger index;
    dispatch_sync(self.queue, ^{
        Contact *contactEntity = (Contact *)contact;
        contactEntity.firstName = firstName;
        contactEntity.lastName = lastName;
        contactEntity.phoneNumber = phoneNumber;
        [self.contacts removeObject:contact];
        index = [self newIndexOfContact:contact];
        [self.contacts insertObject:contact atIndex:index];
    });
    
    return index;
}

#pragma mark - 
- (NSUInteger)newIndexOfContact:(Contact *)contact {
    return [self.contacts indexOfObject:[contact lastName]
                   inSortedRange:NSMakeRange(0, self.contacts.count)
                         options:NSBinarySearchingInsertionIndex
                 usingComparator:^NSComparisonResult(id<ContactProtocol> contact1, id<ContactProtocol> contact2) {
                     return [[contact1 lastName] compare:[contact lastName]];
                 }];
}

@end
