//
//  DataController.h
//  ContactsApp
//
//  Created by Evgeny on 15.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactProtocol.h"

/**
 *  DataController allows to create, update and delete `Contacts` objects.
 */
@interface DataController : NSObject


/**
 *  Accessor for the Data Controller singleton
 *
 *  @return `DataController` singleton
 */
+ (instancetype)sharedInstance;


/**
 *  Returns an array of all `Contact` objects in the store
 *
 *  @return NSArray of contacts
 */
- (NSArray *)allContacts;


/**
 *  Creates a new `Contact` object
 *
 *  @param firstName   first name
 *  @param lastName    last name
 *  @param phoneNumber phone number
 *  @param error       NSError object pointer. Is not nil if the operation is not succesful
 *
 *  @return NSUInteger Index of new object in allContacts array
 */
- (NSUInteger)addNewContactWithFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber error:(NSError **)error;


/**
 *  Updates given `Contact` object
 *
 *  @param contact     `Contact` object to update
 *  @param firstName   first name
 *  @param lastName    last name
 *  @param phoneNumber phone number
 *  @param error       NSError object pointer. Is not nil if the operation is not succesful
 *
 *  @return NSUInteger index of updated contact in the allContact array
 */
- (NSUInteger)updateContact:(id<ContactProtocol>)contact withFirstName:(NSString *)firstName lastName:(NSString *)lastName phoneNumber:(NSString *)phoneNumber error:(NSError **)error;


/**
 *  Deletes given `Contact` object
 *
 *  @param contact `Contact` object to delete
 *  @param error   NSError object pointer. Is not nil if the operation is not succesful
 *
 *  @return NSUInteger index of deletd contact in the allContact array
 */
- (NSUInteger)deleteContact:(id<ContactProtocol>)contact error:(NSError **)error;
@end
