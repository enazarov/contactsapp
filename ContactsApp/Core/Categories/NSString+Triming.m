//
//  NSString+Triming.m
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import "NSString+Triming.h"

@implementation NSString (Triming)
- (instancetype)trimWhitespacesAllowTailingSpace:(BOOL)tailingSpaceAllowed {
    if (self.length==0) {
        return self;
    }
    else {
        NSString *result = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (tailingSpaceAllowed && [self hasSuffix:@" "] && result.length) {
            return [result stringByAppendingString:@" "];
        }
        
        return result;
    }
}

@end
