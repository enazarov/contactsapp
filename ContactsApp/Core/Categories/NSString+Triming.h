//
//  NSString+Triming.h
//  ContactsApp
//
//  Created by Evgeny on 22.05.16.
//  Copyright © 2016 Evgeny Nazarov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Triming)
- (instancetype)trimWhitespacesAllowTailingSpace:(BOOL)tailingSpaceAllowed;
@end
